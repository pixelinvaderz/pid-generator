'''
def get_offset_boundary(anchor, boundary):
    anchor_x, anchor_y = anchor
    boundary_x = boundary[0]
    boundary_y = boundary[1]
    diffx_left = anchor_x - boundary_x
    diffx_right = boundary_x + (boundary[2]) - anchor_x
    diffy_top = anchor_y - boundary_y
    diffy_bottom = boundary_y + (boundary[3]) - anchor_y
    return diffx_left, diffx_right, diffy_top, diffy_bottom
'''

'''
def transform_boundary(startpos, boundary):
    sx, sy = startpos
    diff_x = abs(boundary[0] - sx)
    diff_y = abs(boundary[1] - sy)
    new_boundary = boundary.copy()
    new_boundary[0] = boundary[0] + diff_x
    new_boundary[1] = boundary[1] + diff_y
    return new_boundary
'''
'''
def calc_box_abs_position(anchor_a, anchor_b, boundary_a, boundary_b, from_pos, dir):
    # transform into x64 system
    boundary_an = transform_boundary(from_pos, boundary_a)
    # relative positions of anchor in boundary
    diffx_a_left, diffx_a_right, diffy_a_top, diffy_a_bottom = get_offset_boundary(anchor_a, boundary_a)
    diffx_b_left, diffx_b_right, diffy_b_top, diffy_b_bottom = get_offset_boundary(anchor_b, boundary_b)
    # diff between anchors in different layers
    diffabs_y = diffy_a_bottom + diffy_b_top
    diffabs_x = diffx_a_left - diffx_b_left
    # construct x and y pos of box to be used later
    if dir == 'north_south':
        ax = boundary_an[0] - diffabs_x
        ay = boundary_an[1] + diffy_a_top
        nx = ax + diffx_a_left  # - diffabs_x
        ny = ay + diffy_a_top   # - diffabs_y
        #        return ax, ay, boundary_b[2], boundary_b[3]
        return ax, ay
    if dir == 'west_east':
        ax = boundary_an[0] - diffabs_x
        ay = boundary_an[1] + diffy_a_top
        nx = ax + diffx_a_left  # - diffabs_x
        ny = ay + diffy_a_top  # - diffabs_y
        return ax, ay

    #anchor_an = [sum(x) for x in zip(*la)]
    #anchor_bn = [sum(x) for x in zip(*lb)]
    #anchor_ax, anchor_ay = anchor_an
    #anchor_bx, anchor_by = anchor_bn
'''
'''
def find_anchor_positions(drone):
    path = 'data/atlas/anchor'
    color_head_chassis = (0, 0, 255)
    color_arms_chassis = (255, 0, 0)
    color_tool_arms_chassis_thruster = (0, 255, 0)
    color_emblem = (0, 255, 255)
    start_pos = (22, 21)

    anchor_dict = {}
    for component in ANCHOR:
        if component != 'emblem':
            file = f'drone_{getattr(drone, component)}.png'
            f_name = os.path.join(path, file)
            img = read_from_file(f_name)
        else:
            anchor_dict['emblem_chassis'] = lookup_pixel_cord(img, mask_color=color_emblem)
        if component == 'head':
            # apply head to chassis
            anchor_dict['head_chassis'] = lookup_pixel_cord(img, mask_color=color_head_chassis)
        # find chassis thruster junction
        elif component == 'chassis':
            # tools - arms
            chassis_arms = lookup_pixel_cord(img, mask_color=color_tool_arms_chassis_thruster)
        elif component == 'arms':
            # arms - chassis

            arms_chassis = lookup_pixel_cord(img, mask_color=color_arms_chassis)
            anchor_arms_left = arms_chassis[0]
            anchor_chassis_left = arms_chassis[1]
            anchor_arms_right = arms_chassis[2]
            anchors_chassis_right = arms_chassis[3]
            diff_x_left_arm, _, _, _ = get_offset_boundary(anchor_arms_left, PIXEL_BOUNDARIES['left_arm'])
            _, diff_x_chassis, _, _ = get_offset_boundary(anchor_chassis_left, PIXEL_BOUNDARIES['chassis'])
            diff = diff_x_left_arm - diff_x_chassis
            abs_pos = [start_pos, (PIXEL_BOUNDARIES['chassis'][2] - diff, 0)]
            abs_pos = [sum(x) for x in zip(*abs_pos)]
            #abs_pos = calc_box_abs_position(anchor_arms_left, anchor_chassis_left, PIXEL_BOUNDARIES['left_arm'],
            #                                PIXEL_BOUNDARIES['chassis'], from_pos=(22, 21), dir='west_east')
            anchor_dict['arms_chassis_left'] = [abs_pos]

        elif component == 'thruster':
            chassis_thruster = lookup_pixel_cord(img, mask_color=color_tool_arms_chassis_thruster)
            anchor_chassis = chassis_thruster[2]
            anchor_thruster = chassis_thruster[3]
            # diff bottom
            _, _, _, diff_y_chassis = get_offset_boundary(PIXEL_BOUNDARIES['chassis'], anchor_chassis)
            # diff top
            _, _, diff_y_thruster, _ = get_offset_boundary(PIXEL_BOUNDARIES['thruster'], anchor_thruster)
            diff = diff_y_chassis + diff_y_thruster
            abs_pos = [start_pos, (0, PIXEL_BOUNDARIES['chassis'][3] - diff)]
            abs_pos = [sum(x) for x in zip(*abs_pos)]
            #abs_pos = calc_box_abs_position(anchor_chassis, anchor_thruster, PIXEL_BOUNDARIES['chassis'],
            #                                PIXEL_BOUNDARIES['thruster'], from_pos=(22, 21), dir='north_south')
            anchor_dict['chassis_thruster'] = [abs_pos]
    return anchor_dict
'''

'''
def get_weld_coords(drone):
    anchor_positions = get_anchor_positions(drone)
    anchor_dict = {}
    # body - thruster
    ch_th = anchor_positions['chassis_thruster']
    th = anchor_positions['thruster']
    print(ch_th, th)
    return
'''
