import os

import numpy as np
from PIL import Image

from data.constants import PIXEL_BOUNDARIES

PATH = './data/atlas/'
CROP_PATH = './data/atlas/components/'
ENDING = '.png'

for file in os.listdir(PATH):
    if file.endswith(ENDING):
        image = Image.open(PATH + file)
        print(file)
        for key in PIXEL_BOUNDARIES.keys():
            boundaries = np.array(PIXEL_BOUNDARIES[key])
            boundaries[2] = boundaries[0] + boundaries[2]
            boundaries[3] = boundaries[1] + boundaries[3]
            boundaries = tuple(boundaries)
            crop = image.crop(boundaries)
            atlas_name = file.replace(ENDING, '')
            file_name = CROP_PATH + key + '_' + atlas_name + '.png'
            crop.save(file_name)
            print(file_name)