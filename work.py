from generator.generate_drone import Drone
from generator.generate_drone import reset_uid
from image_processor.build_image import *

import numpy as np
import json


def write_json(json_dict):
    with open('data/drone.json', 'w') as f:
        json.dump(json_dict, f)


np.random.seed(1000)

# reset UID if neededcd opm
res_uid = True
if res_uid:
    reset_uid()

# generate a drone
drone = Drone(12)
drone_json = drone.build_json()
print(drone_json)
write_json(drone_json)
image_component_dict = get_image_components(drone)
color_component_dict = get_image_colormaps(drone)

'''
# derzeit merge mit image on demand
# TODO change to real code above
drone = Drone(1000)
drone.arms = 'economy_1'
drone.head = 'economy_1'
drone.thruster = 'economy_1'
drone_economy_json = drone.build_json()
print(drone_economy_json)


image_component_dict = get_image_components(drone)
#
color_component_dict = get_image_colormaps(drone)
'''
# recolor body parts
recolored_body_parts_dict = blend_body_parts(drone, image_component_dict, color_component_dict)
#
drone_image = weld_drone(drone, image_component_dict, recolored_body_parts_dict)

show = True
if show:
    drone_image.show()
save = True
if save:
    drone_image.save(f'data/image/{drone.uid}_{drone.dominant_sector}.png')

