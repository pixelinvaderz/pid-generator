from generator.generate_drone import Drone
# from generator.generate_drone import reset_uid
import argparse
import sys

import numpy as np
import json


def write_json(json_dict, name ):
    # with open(f'data/{name}', 'w') as f:
    with open(f'{name}', 'w') as f:
        json.dump(json_dict, f)


def print_to_stdout(*a):
    # Here a is the array holding the objects
    # passed as the argument of the function
    print(*a, file=sys.stdout)



np.random.seed(1000)

# reset UID if needed
# res_uid = True
# if res_uid:
#    reset_uid()

parser = argparse.ArgumentParser()
parser.add_argument('--uid', help='unique drone ID')
args = parser.parse_args()
# generate a drone
drone = Drone(int(args.uid))
save_name = f'{args.uid}.json'
drone_json = drone.build_json()
# write_json(drone_json, save_name)
print_to_stdout(drone_json)
