import sys

from image_processor.build_image import *
from generator.generate_drone import Drone
import json
import argparse

#
save_to_stdout = False
save = True
show = False
#
parser = argparse.ArgumentParser()
parser.add_argument('--f_name', help='JSON file name for drone')
args = parser.parse_args()

with open(args.f_name) as json_file:
    json_data = json.load(json_file)
drone = Drone(json_data['id'])
image_component_dict = get_image_components(drone)
color_component_dict = get_image_colormaps(drone)
# recolor body parts
recolored_body_parts_dict = blend_body_parts(drone, image_component_dict, color_component_dict)
#
drone_image = weld_drone(drone, image_component_dict, recolored_body_parts_dict)
if show:
    drone_image.show()
if save:
    drone_image.save(f'data/image/{drone.uid}_{drone.dominant_sector}.png')
if save_to_stdout:
    drone_image.save(sys.stdout, 'png')
