ATLAS = ['economy_1', 'economy_2', 'military_1', 'military_2', 'science_1', 'science_2', 'society_1', 'society_2']
TYPE = ['economy', 'military', 'science', 'society']
COMPONENT = ['head', 'chassis', 'arms', 'left_tool', 'right_tool', 'thruster']
PRIORITY_HALO = ['head', 'left_arm', 'right_arm', 'left_tool', 'right_tool', 'chassis', 'thruster']
ANCHOR = ['head', 'chassis', 'arms', 'thruster', 'emblem']
DETAIL_COMPONENT = ['head', 'chassis', 'left_arm', 'right_arm', 'left_tool', 'right_tool', 'thruster']
RARITY = [0.60, 0.25, 0.10, 0.05]
RARITY_SCALE = ['common', 'uncommon', 'rare', 'epic']
COLOR_SOCIETY = ['#C03F65', '#C03FBB', '#8C3FC0', '#3FC098', '#3FC051']
COLOR_ECONOMY = ['#BF4040', '#BF6940', '#BF8740', '#3F73C0', '#3FA0C0']
COLOR_MILITARY = ['#C0993F', '#C0B73F', '#8FC03F', '#50C03F', '#3FC06F']
COLOR_SCIENCE = ['#483FC0', '#3F6FC0', '#3FA7C0', '#3FC0A7', '#3FC060']
COLORS_CHASSIS = {'society': COLOR_SOCIETY, 'economy': COLOR_ECONOMY, 'military': COLOR_MILITARY,
                  'science': COLOR_SCIENCE}
THRUSTER_COLORS = ['#BF4040', '#BF6140', '#BF7F40', '#BFBB40', '#79BF40', '#40BF4D', '#40BFA3', '#4094BF',
                   '#4042BF', '#7D40BF', '#BF40A5']
EYE_COLORS = ['#c5131c', '#fd6400', '#b3500e', '#be8034', '#c1c413', '#3bd952', '#468508', '#17c98a', '#3ab3ac',
              '#145fbe', '#020f59', '#e782c6', '#ca1564', '#8720b9', '#bed5e1', '#737373']
RARITY_COLORS_DICT = {'common': '#45475C', 'uncommon': '#317065', 'rare': '#5E3DB1', 'epic': '#C4AC30'}
#
PIXEL_BOUNDARIES = {'head': [13, 0, 27, 13], 'chassis': [17, 13, 19, 18], 'left_arm': [0, 10, 13, 21],
                    'right_arm': [40, 10, 13, 21], 'left_tool': [0, 31, 13, 14], 'right_tool': [40, 31, 13, 14],
                    'thruster': [17, 31, 19, 14]}
ANCHOR_COLORS = {'head': (0, 0, 255), 'left_arm_chassis': (255, 0, 0), 'right_arm_chassis': (255, 0, 0),
                 'head_chassis': (0, 0, 255),
                 'chassis': (255, 0, 0),
                 'chassis_thruster': (0, 255, 0),
                 'left_arm': (0, 255, 0),
                 'left_tool_left_arm': (0, 255, 0),
                 'right_arm': (0, 255, 0),
                 'right_tool_right_arm': (0, 255, 0),
                 'thruster': (0, 255, 0),
                 'emblem': (0, 255, 255)}

ATLAS_SIZE = [(53, 45)]
IMAGE_TARGET_SIZE = [(64, 64)]
