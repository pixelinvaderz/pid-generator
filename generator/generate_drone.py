import json
import numpy as np
from collections import defaultdict
from data.constants import *


def generate_uid():
    with open('data/uid/counter.json') as f:
        data = json.load(f)
    data['drone_count'] = data['drone_count'] + 1
    uid = data['drone_count']
    with open('data/uid/counter.json', 'w') as f:
        json.dump(data, f)
    return uid


def reset_uid():
    with open('data/uid/counter.json') as f:
        data = json.load(f)
    data['drone_count'] = 0
    with open('data/uid/counter.json', 'w') as f:
        json.dump(data, f)


def draw_uniform_from_atlas(ATLAS):
    size = len(ATLAS)
    number = np.random.randint(0, size)
    return ATLAS[number]


def get_max_value(data):
    d = defaultdict(list)
    for key, value in data.items():
        d[value].append(key)
    return max(d.items())[1]


def determine_color(colors_list):
    num_colors = len(colors_list)
    selected_color_index = np.random.randint(0, num_colors)
    color = colors_list[selected_color_index]
    return color


def calculate_drone_component_rarity(RARITY):
    number = np.random.uniform(0, 1)
    common = RARITY[0]
    uncommon = RARITY[0] + RARITY[1]
    rare = RARITY[0] + RARITY[1] + RARITY[2]
    epic = 1.0

    if number <= common:
        rarity = 'Common'
        return rarity
    if common < number <= uncommon:
        rarity = 'Uncommon'
        return rarity
    if uncommon < number <= rare:
        rarity = "Rare"
        return rarity
    if rare < number <= epic:
        rarity = 'Epic'
        return rarity


class Drone():
    def __init__(self, uid=0, emblem='tier1'):
        # if uid == 0:
        #    self.uid = generate_uid()
        # else:
        self.uid = uid
        # seed for random components is dependent on UID
        np.random.seed(self.uid)
        # define attribute from COMPONENT constant
        for component in COMPONENT:
            # locals()[component] = component
            selected_component = draw_uniform_from_atlas(ATLAS)
            setattr(self, component, selected_component)
            rarity = component + '_rarity'
            # locals()[rarity] = rarity
            setattr(self, rarity, calculate_drone_component_rarity(RARITY))
        # find dominant sector
        self.dominant_sector = self.determine_dominant_drone_type(TYPE)
        # find colors for chassis
        self.chassis_color = determine_color(COLORS_CHASSIS[self.dominant_sector])
        self.thruster_color = determine_color(THRUSTER_COLORS)
        self.eye_color = determine_color(EYE_COLORS)
        self.emblem = emblem

    def determine_dominant_drone_type(self, TYPE):
        type_dict = {i: 0 for i in TYPE}
        for typ in TYPE:
            for attribute in COMPONENT:
                if getattr(self, attribute).find(typ) != -1:
                    type_dict[typ] += 1
        # find max and if it is unique
        max_keys = get_max_value(type_dict)
        key_size = len(max_keys)
        if key_size > 1:
            number = np.random.randint(0, key_size)
            dominant_sector = max_keys[number]
        else:
            dominant_sector = max_keys[0]
        # print(type_dict, max_keys)
        return dominant_sector

    def build_json(self):
        component_dict = {}
        for component in COMPONENT:
            component_dict[component] = {'type': getattr(self, component),
                                         'sector': self.determine_drone_type(component),
                                         'rarity': getattr(self, component + '_rarity')}
        json_dict = {'id': self.uid, 'components': component_dict, 'dominant_sector': self.dominant_sector,
                     'chassis_color': self.chassis_color, 'thruster_color': self.thruster_color, 'eye_color': self.eye_color}
        return json_dict

    def determine_drone_type(self, component):
        for attribute in COMPONENT:
            if component == attribute:
                atr = getattr(self, attribute)
                for typ in TYPE:
                    if atr.find(typ) != -1:
                        return typ
