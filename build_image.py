import json
import argparse
import sys


from image_processor.build_image import *
from generator.generate_drone import Drone

parser = argparse.ArgumentParser()
parser.add_argument('--json', help='JSON string for building drone')
args = parser.parse_args()


#
#print(args)

json_data = json.loads(args.json)
drone = Drone(json_data['id'])
image_component_dict = get_image_components(drone)
color_component_dict = get_image_colormaps(drone)
# recolor body parts

recolored_body_parts_dict = blend_body_parts(drone, image_component_dict, color_component_dict)
drone_image = weld_drone(drone, image_component_dict, recolored_body_parts_dict)
#drone_image.show()
drone_image.save(f'{drone.uid}.png')
print(f'{drone.uid}.png')
#drone_image.save(sys.stdout, 'png')


