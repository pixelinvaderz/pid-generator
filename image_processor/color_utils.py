from PIL import Image, ImageColor

def pixel_matches_color_mask(pixel: [int], mask_color: (int, int, int)):
    r, g, b = mask_color
    does_match = True
    if r and pixel[0] != r:
        does_match = False
    if g and pixel[1] != g:
        does_match = False
    if b and pixel[2] != b:
        does_match = False
    return does_match


def compare_color_mask(pixel: [int], mask_color: (int, int, int)):
    r, g, b = mask_color
    does_match = False
    if r == pixel[0] and g == pixel[1] and b == pixel[2]:
        does_match = True
    return does_match


def lookup_pixel_cord(img: Image, mask_color: (int, int, int)):
    pixels = []
    for i in range(img.size[0]):
        for j in range(img.size[1]):
            pixel = img.getpixel((i, j))
            #if pixel_matches_color_mask(pixel, mask_color):
            if compare_color_mask(pixel, mask_color):
                pixels.append((i, j))
    return pixels


def multiply_alpha(img: Image, alpha: float):
    if alpha < 1:
        for i in range(img.size[0]):
            for j in range(img.size[1]):
                (r, g, b, a) = img.getpixel((i, j))
                img.putpixel((i, j), (r, g, b, int(a * alpha)))
