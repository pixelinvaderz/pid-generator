import os
from os.path import exists
from blendmodes.blend import blendLayers, BlendType
from data.constants import *
from image_processor.color_utils import *
from image_processor.blend_color import blend_bodypart_color
import numpy as np


def read_from_file(f_name):
    file_exists = exists(f_name)
    if file_exists:
        try:
            pic = Image.open(f_name)
            return pic
        except FileNotFoundError:
            print(f'could not open file {f_name}')
    raise ValueError(f'file {f_name} not found!')


def get_component(what, drone, drone_part, path='data/atlas/'):
    if what == 'background':
        path = 'data/backgrounds/'
        f_name = os.path.join(path, f'{drone.dominant_sector}.png')
        img = read_from_file(f_name)
        return img.convert('RGBA')
        # geht auch return img.convert('RGBA')
    elif what == 'emblem':
        path = 'data/emblems/'
        f_name = os.path.join(path, f'drone_{drone.emblem}.png')
        img = read_from_file(f_name)
        return img.convert('RGBA')
    else:
        f_name = os.path.join(path, f'drone_{drone_part}.png')
        atlas = read_from_file(f_name)
        atlas = atlas.convert('RGBA')
        # cutting from atlas
        box = PIXEL_BOUNDARIES[what]
        # crop, note that 3. 4. parameter means width and height of box in pixels!!
        component = atlas.crop((box[0], box[1], box[0] + box[2], box[1] + box[3]))
        return component


def get_image_colormaps(drone):
    drone_image_colormaps = {'head': get_component('head', drone, drone.head, path='data/atlas/color'),
                             'left_arm': get_component('left_arm', drone, drone.arms, path='data/atlas/color'),
                             'right_arm': get_component('right_arm', drone, drone.arms, path='data/atlas/color'),
                             'chassis': get_component('chassis', drone, drone.chassis, path='data/atlas/color'),
                             'left_tool': get_component('left_tool', drone, drone.left_tool, path='data/atlas/color'),
                             'right_tool': get_component('right_tool', drone, drone.right_tool,
                                                         path='data/atlas/color'),
                             'thruster': get_component('thruster', drone, drone.thruster, path='data/atlas/color'),
                             }
    return drone_image_colormaps


def get_image_components(drone):
    drone_image_components = {'head': get_component('head', drone, drone_part=drone.head),
                              'left_arm': get_component('left_arm', drone, drone_part=drone.arms),
                              'right_arm': get_component('right_arm', drone, drone_part=drone.arms),
                              'chassis': get_component('chassis', drone, drone_part=drone.chassis),
                              'left_tool': get_component('left_tool', drone, drone_part=drone.left_tool),
                              'right_tool': get_component('right_tool', drone, drone_part=drone.right_tool),
                              'thruster': get_component('thruster', drone, drone_part=drone.thruster),
                              'background': get_component('background', drone, drone_part=drone.dominant_sector),
                              'emblem': get_component('emblem', drone, drone_part=drone.emblem)}
    return drone_image_components


def get_anchor_bodypart(drone, component, color, boundary):
    path = 'data/atlas/anchor'
    if component == 'left_arm' or component == 'right_arm':
        component = 'arms'
    file = f'drone_{getattr(drone, component)}.png'
    f_name = os.path.join(path, file)
    img = read_from_file(f_name)
    # crop
    part = img.crop((boundary[0], boundary[1], boundary[0] + boundary[2], boundary[1] + boundary[3]))
    # find
    cord = lookup_pixel_cord(part, mask_color=color)
    #
    return cord


def get_anchor_positions(drone):
    anchor_positions = {}
    # build dict with boundaries and colors
    for component in ANCHOR_COLORS:
        color = ANCHOR_COLORS[component]
        body_part = component
        if component == 'emblem' or component == 'head_chassis' or component == 'arms_chassis' \
                or component == 'chassis_thruster':
            boundary = PIXEL_BOUNDARIES['chassis']
            body_part = 'chassis'
        elif component == 'left_arm_chassis':
            boundary = PIXEL_BOUNDARIES['left_arm']
            body_part = 'arms'
        elif component == 'left_tool_left_arm':
            boundary = PIXEL_BOUNDARIES['left_tool']
            body_part = 'left_tool'
        elif component == 'right_tool_right_arm':
            boundary = PIXEL_BOUNDARIES['right_tool']
            body_part = 'right_tool'
        elif component == 'right_arm_chassis':
            boundary = PIXEL_BOUNDARIES['right_arm']
            body_part = 'arms'
        else:
            boundary = PIXEL_BOUNDARIES[component]
        anchor_positions[component] = get_anchor_bodypart(drone, body_part, color, boundary)
    return anchor_positions


def get_emblem_position(start_pos, emblem: Image, emblem_anchor):
    offset = int(emblem.size[0] / 2)
    emblem_x = start_pos[0] + emblem_anchor[0][0] - offset
    emblem_y = start_pos[1] + emblem_anchor[0][1] - offset
    return emblem_x, emblem_y


def transform_boundary(start_pos, boundary: list, anchor_positions, component1, component2):
    if component2 == 'head_chassis':
        anchor1 = anchor_positions[component1]
    else:
        anchor1 = anchor_positions[component1 + '_' + component2]
    anchor2 = anchor_positions[component2]
    if component1 == 'right_arm':
        anchor2 = [(anchor2[1][0], anchor2[1][1])]

    diff_x = anchor2[0][0] - anchor1[0][0]
    diff_y = anchor2[0][1] - anchor1[0][1]
    if component2 == 'thruster':
        new_boundary = [
            start_pos[0] + diff_x,
            start_pos[1] - diff_y,
            boundary[2],
            boundary[3]
        ]
    elif component1 == 'left_tool' or component1 == 'right_tool':
        new_boundary = [
            start_pos[0],
            start_pos[1],
            boundary[2],
            boundary[3]
        ]
    else:
        new_boundary = [
            start_pos[0] + diff_x,
            start_pos[1] + diff_y,
            boundary[2],
            boundary[3]
        ]

    '''
    new_boundary = boundary.copy()
    diff_x = -anchor1[0][0] + anchor2[0][0]
    diff_y = anchor1[0][1] - anchor2[0][1]
    new_boundary[0] = start_pos[0] + diff_x
    # TODO why?, this part needs a rework
    if component1 == 'head' or component1 == 'left_tool' or component1 == 'right_tool':
        new_boundary[1] = start_pos[1] - diff_y
    else:
        new_boundary[1] = start_pos[1] + diff_y
    '''
    return new_boundary


def blend_body_parts(drone, image_component_dict, color_component_dict):
    #
    red = (255, 0, 0)
    green = (0, 255, 0)
    blue = (0, 0, 255)
    recolored_components = {}
    for body_part in DETAIL_COMPONENT:
        if body_part == 'thruster':
            recolored_components[body_part] = blend_bodypart_color(image_component_dict[body_part],
                                                                   color_component_dict[body_part], blue,
                                                                   drone.thruster_color, body_part, opacity=0.5)
        else:
            # green mask
            green_ready = blend_bodypart_color(image_component_dict[body_part],
                                               color_component_dict[body_part], green,
                                               drone.chassis_color, body_part)
            # red mask
            recolored_components[body_part] = blend_bodypart_color(green_ready,
                                                                   color_component_dict[body_part], red,
                                                                   drone.eye_color, body_part)
    return recolored_components


def boundary_boxes_for_pixel(pixel, boundaries):
    found = {}
    for boundary in boundaries:
        bound = boundaries[boundary]
        if bound[0][0] + bound[0][2] - 1 >= pixel[0] >= bound[0][0] and bound[0][1] <= pixel[1] <= bound[0][1] + bound[0][3] - 1:
            found[boundary] = boundaries[boundary]
    return found


def not_background(img, background, pixel):
    return not is_background(img, background, pixel)


def is_background(img, background, pixel):
    x = pixel[0]
    y = pixel[1]
    pixel_drone = img.getpixel((x, y))
    pixel_background = background.getpixel((x, y))
    if pixel_drone != pixel_background:
        return False
    else:
        return True


def build_rarity_image(background, img, boundaries):
    rarity_img = Image.new(mode="RGBA", size=(64, 64))
    x_range = range(0, img.size[0])
    y_range = range(0, img.size[1])
    for body_part in PRIORITY_HALO:
        #
        for x in x_range:
            for y in y_range:
                # if we found something
                if not_background(img, background, (x, y)):
                    findings = boundary_boxes_for_pixel((x, y), boundaries)
                    if findings and body_part in findings.keys():
                        col = findings[body_part][1]
                        hexcolor = RARITY_COLORS_DICT[col.lower()]
                        color = ImageColor.getcolor(hexcolor, "RGBA")
                        for i in range(-1, 2):
                            for j in range(-1, 2):
                                x1 = x + i
                                y1 = y + j
                                x1 = max(0, min(x1, img.size[0] - 1))
                                y1 = max(0, min(y1, img.size[1] - 1))
                                if is_background(img, background, (x1, y1)):
                                    rarity_img.putpixel((x1, y1), color)
    return rarity_img


def weld_drone(drone, image_component_dict, recolored_body_parts_dict: dict):
    stored_boundaries = {}
    start_pos = (22, 21)
    # start_pos_thruster = (22, 36)
    anchor_positions = get_anchor_positions(drone)
    # fetch background
    background = image_component_dict['background']
    # thruster - chassis
    thruster_boundary = transform_boundary(start_pos, PIXEL_BOUNDARIES['thruster'], anchor_positions,
                                           'chassis', 'thruster')
    total_black = Image.new(mode="RGBA", size=(64, 64))
    temp_image = total_black.copy()
    # store boundary
    stored_boundaries['thruster'] = [thruster_boundary, drone.thruster_rarity]
    #
    thruster_img = recolored_body_parts_dict['thruster']
    temp_image.paste(thruster_img, box=(thruster_boundary[0], thruster_boundary[1]), mask=thruster_img)
    # left arm - chassis
    left_arm_boundary = transform_boundary(start_pos, PIXEL_BOUNDARIES['left_arm'], anchor_positions,
                                           'left_arm', 'chassis')
    # store boundary
    stored_boundaries['left_arm'] = [left_arm_boundary, drone.arms_rarity]
    #
    left_arm_img = recolored_body_parts_dict['left_arm']
    temp_image.paste(left_arm_img, box=(left_arm_boundary[0], left_arm_boundary[1]), mask=left_arm_img)
    # right arm - chassis
    right_arm_boundary = transform_boundary(start_pos, PIXEL_BOUNDARIES['right_arm'], anchor_positions,
                                            'right_arm', 'chassis')
    # store boundary
    stored_boundaries['right_arm'] = [right_arm_boundary, drone.arms_rarity]
    #
    right_arm_img = recolored_body_parts_dict['right_arm']
    temp_image.paste(right_arm_img, box=(right_arm_boundary[0], right_arm_boundary[1]), mask=right_arm_img)
    # chassis
    # store boundary
    stored_boundaries['chassis'] = [[22, 21, 19, 18], drone.chassis_rarity]
    chassis_img = recolored_body_parts_dict['chassis']
    temp_image.paste(chassis_img, box=start_pos, mask=chassis_img)
    #
    # old but working versions
    # left_tool_start_x_old = start_pos[0] - (start_pos[0] - left_tool_boundary[0]) -
    # (start_pos[0] - left_arm_boundary[0])
    # better solution and better to understand
    left_tool_start_x = left_arm_boundary[0] + anchor_positions['left_arm'][0][0] - \
                        anchor_positions['left_tool_left_arm'][0][0]
    left_tool_start_y = left_arm_boundary[1] + anchor_positions['left_arm'][0][1]
    # left tool - left arm
    left_tool_boundary = transform_boundary((left_tool_start_x, left_tool_start_y), PIXEL_BOUNDARIES['left_tool'],
                                            anchor_positions,
                                            'left_tool', 'left_arm')
    # store boundary
    stored_boundaries['left_tool'] = [left_tool_boundary, drone.left_tool_rarity]
    left_tool_img = recolored_body_parts_dict['left_tool']
    temp_image.paste(left_tool_img, box=(left_tool_start_x, left_tool_start_y), mask=left_tool_img)

    # old but working version
    # right_tool_start_x = start_pos[0] - (start_pos[0] - right_tool_boundary[0]) - (start_pos[0] - right_arm_boundary[0])
    # better solution
    right_tool_start_x = right_arm_boundary[0] + anchor_positions['right_arm'][0][0] - \
                         anchor_positions['right_tool_right_arm'][0][0]
    right_tool_start_y = right_arm_boundary[1] + anchor_positions['right_arm'][0][1]
    # right tool - right arm
    right_tool_boundary = transform_boundary((right_tool_start_x, right_tool_start_y), PIXEL_BOUNDARIES['right_tool'],
                                             anchor_positions,
                                             'right_tool', 'right_arm')
    # store boundary
    stored_boundaries['right_tool'] = [right_tool_boundary, drone.right_tool_rarity]
    #
    right_tool_img = recolored_body_parts_dict['right_tool']
    temp_image.paste(right_tool_img, box=(right_tool_start_x,
                                          right_tool_start_y), mask=right_tool_img)

    # emblem - chassis
    emblem_img = image_component_dict['emblem']
    emblem_pos = get_emblem_position(start_pos, emblem_img, anchor_positions['emblem'])
    temp_image.paste(emblem_img, box=(emblem_pos[0], emblem_pos[1]), mask=emblem_img)
    # chassis - head
    head_boundary = transform_boundary(start_pos, PIXEL_BOUNDARIES['head'], anchor_positions,
                                       'head', 'head_chassis')
    # stor boundary
    stored_boundaries['head'] = [head_boundary, drone.head_rarity]
    #
    head_img = recolored_body_parts_dict['head']
    temp_image.paste(head_img, box=(head_boundary[0], head_boundary[1]), mask=head_img)
    # find rarity hal
    rarity_img = build_rarity_image(total_black, temp_image, stored_boundaries)
    # rarity_img.show()
    multiply_alpha(rarity_img, 0.55)
    full_welded = blendLayers(background, rarity_img, BlendType.ADDITIVE)
    full_welded.paste(thruster_img, box=(thruster_boundary[0], thruster_boundary[1]), mask=thruster_img)
    full_welded.paste(left_arm_img, box=(left_arm_boundary[0], left_arm_boundary[1]), mask=left_arm_img)
    full_welded.paste(right_arm_img, box=(right_arm_boundary[0], right_arm_boundary[1]), mask=right_arm_img)
    full_welded.paste(chassis_img, box=start_pos, mask=chassis_img)
    full_welded.paste(left_tool_img, box=(left_tool_start_x, left_tool_start_y), mask=left_tool_img)
    full_welded.paste(right_tool_img, box=(right_tool_start_x, right_tool_start_y), mask=right_tool_img)
    full_welded.paste(emblem_img, box=(emblem_pos[0], emblem_pos[1]), mask=emblem_img)
    full_welded.paste(head_img, box=(head_boundary[0], head_boundary[1]), mask=head_img)
    return full_welded
