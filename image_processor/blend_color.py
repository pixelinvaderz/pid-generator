from PIL import Image, ImageColor
from blendmodes.blend import blendLayers, BlendType
from image_processor.color_utils import pixel_matches_color_mask
from image_processor.color_utils import multiply_alpha



def find_color(by_hsv, skin_color):
    for color in by_hsv:
        value, name = color
        if name == skin_color:
            return value
    return None


def recolor(img: Image, color, mode="RGBA"):
    img = img.convert(mode)
    for i in range(img.size[0]):  # for every pixel:
        for j in range(img.size[1]):
            # use mask with color
            if img.getpixel((i, j)) != (0, 0, 0, 0):
                img.putpixel((i, j), color)
    return img


def extract_rgb_layer_from_mask(img: Image, mask: Image, mask_color: (int, int, int), mode="RGBA"):
    layer = Image.new(mode, img.size)
    img = img.convert(mode)
    for i in range(img.size[0]):  # for every pixel:
        for j in range(img.size[1]):
            # use mask with color
            pixel = mask.getpixel((i, j))
            if pixel_matches_color_mask(pixel, mask_color):
                layer.putpixel((i, j), img.getpixel((i, j)))
    return layer


def blend_bodypart_color(body_part: Image, character_util: Image, mask_color, color_code: str, body_part_string, opacity=1):
    # convert hex to rgb
    body_element_color = ImageColor.getrgb(color_code)

    back_layer = extract_rgb_layer_from_mask(body_part, character_util, mask_color)
    fore_layer = recolor(back_layer, body_element_color)

    # incorrect versions !!!
    # fore_layer.putalpha(int(255*opacity))
    # this is the correct versions

    if body_part_string == 'thruster':
        multiply_alpha(fore_layer, 0.5)
    else:
        multiply_alpha(fore_layer, 1.0)
    # body_part = blendLayers(body_part, fore_layer, BlendType.HUE)
    body_part = blendLayers(body_part, fore_layer, BlendType.COLOUR)

    show = False
    if show:
        back_layer.show()
        fore_layer.show()
        body_part.show()
    return body_part
